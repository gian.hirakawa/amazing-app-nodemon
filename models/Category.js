const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = require('mongoose').Schema;

//Mongoose Schema Builder

const CategorySchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	}
});

module.exports = mongoose.model('category', CategorySchema);