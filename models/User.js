const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = require('mongoose').Schema;

//Mongoose Schema Builder

const UserSchema = new Schema({
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	}
});

//Pre-hook Get plain text pass and hashing it then storing it
// No arrow functions when there's this within the codeblock
UserSchema.pre('save', async function(next){
	const user = this;
	const hash =  await bcrypt.hash(this.password, 10);
	this.password = hash;
	next();
});

UserSchema.methods.isValidPassword = async function(password){
	const user = this;
	const compare = await bcrypt.compare(password, user.password);
	return compare;
}

module.exports = mongoose.model('user', UserSchema);