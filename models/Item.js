const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = require('mongoose').Schema;

//Mongoose Schema Builder

const ItemSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},	
	stock: {
		type: Number,
		required: true
	},
	category: {
		type: String,
		required: true
	}

});

module.exports = mongoose.model('item', ItemSchema);