const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const router = express.Router();

router.post('/signup', 
	passport.authenticate('signup', {session:false}),
	async (req,res,next) => {
		// res.status(200).send('hello');
		res.json({
			message : 'Signup successful',
			user : req.user
		});
	}
	// (req,res,next) => {
	// 	User.create(req.body)
	// 		.then(user => {
	// 			res.send(user);
	// 		}).catch(next);
	// }
);

router.post('/login', async (req,res,next) => {
	passport.authenticate('login', async (err,user,info) => {
		try{
			if(err || !user){
				const error = new Error('An error occured');
				return next(error);
			}
			req.login(user, {session:false},async(error)=> {
				if(error) return next(error);
				const body = {_id : user._id, email : user.email};
				const token = jwt.sign({user: body}, 'sikretKi');
				return res.json({token});
			});
		}catch (error) {
			return next(error);
		}
	})(req,res,next);
});

module.exports = router;