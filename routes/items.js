const express = require('express');
const router = express.Router();
const Item = require('../models/Item');

router.post('/add', (req,res,next)=>{
	Item.create(req.body)
	.then(item => {
		res.send(item);
	}).catch(next);
});

router.get('/', (req,res,next)=>{
	Item.find()
	.then(item => {
		res.send(item);
	}).catch(next);
})

module.exports = router;