const express = require('express');
const router = express.Router();
const Category = require('../models/Category');

router.post('/create', (req,res,next)=>{
	Category.create(req.body)
	.then(category => {
		res.send(category);
	}).catch(next);
});

router.get('/', (req,res,next)=>{
	Category.find({})
	.then(category => {
		res.send(category);
		// console.log(category);
	}).catch(next);
})

module.exports = router;