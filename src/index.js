const express = require('express');
const config = require('./config');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const userRouter = require('../routes');
const categoryRouter = require('../routes/categories');
const itemRouter = require('../routes/items');
const passport = require('passport');
const cors = require('cors');
const app = express();
require('../auth/index');



mongoose.connect('mongodb+srv://admin0:admin1@cluster0-9pwn2.mongodb.net/amazing_app_db?retryWrites=true', {useNewUrlParser: true});

app.use(bodyParser.json());
app.use(cors());

app.use('/reg', userRouter);

app.use('/categories', categoryRouter);
app.use('/items', itemRouter);

app.use((err, req, res, next) => {
	res.status(422).send({error: err.message});
});

app.get('/userprofile', passport.authenticate('jwt', {session:false}), (req,res,next)=>{
	res.status(200).send('user profile');
});

app.post('/helloworld', (req,res)=>{
	res.status(200).send(req.body)
});

app.listen(config.port, () => {
	console.log(`Listening at http://localhost:${config.port}`);
});