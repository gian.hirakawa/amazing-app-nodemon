const User = require('../models/User');
const localStrategy = require('passport-local').Strategy;
const passport = require('passport');
const passportJwt = require('passport-jwt');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use('signup', new localStrategy({
	usernameField: 'email',
	passwordField: 'password'
}, async (email,password, done) => {
	try{
		const user = await User.create({email,password});
		return done(null, user);
	}catch (error){
		done(error);
	}
}));

passport.use('login', new localStrategy({
	usernameField : 'email',
	passwordField : 'password'
}, async (email,password,done) => {
	try{
		const user = await User.findOne({email});
		if(!user){
			return done(null,false,{message:'User not found'});
		}
		const validate = await user.isValidPassword(password);
		// console.log(validate);
		if(!validate){
			return done(null,false,{message:'Wrong Password'});
		}
		return done(null, user, {message: 'Logged in Successfully'});
	}catch (error) {
		return done(error);		
	}
}));

passport.use(new JWTstrategy({
	secretOrKey : 'sikretKi',
	jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken()
}, async (jwt_payload, done) => {
	try {
		return done(null, jwt_payload.user);
	}catch (error){
		done(error);
	}
}));

//no need for module exports
//just copy paste require('../auth/index') into the main index
